/*
 * pilerreimport.c, MV
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include <locale.h>
#include <syslog.h>
#include <getopt.h>
#include <piler.h>


extern char *optarg;
extern int optind;

int dryrun = 0;
int reimportall = 0;
int rc = 0;
char *query=NULL;
int verbosity = 0;
int max_matches = 1000;
char *index_list = "main1,dailydelta1,delta1";
regex_t regexp;
FILE * logfile;


int store_recipients(struct session_data *sdata, struct __data *data, char *to, uint64 id, struct __config *cfg);
int update_metadata_reference(struct session_data *sdata, struct parser_state *state, struct __data *data, char *ref, struct __config *cfg);
int reimport_emails_matching_to_query(struct session_data *export_sdata, struct session_data *import_sdata, struct __data *data, char *s, struct __config *cfg);


void usage(){
   printf("\nusage: pilerreimport\n\n");

   printf("    [-c <config file>]                Config file to use if not the default\n");
   printf("    -a <start date>                   Start date in YYYY.MM.DD format\n");
   printf("    -b <stop date>                    Stop date in YYYY.MM.DD format\n");
   printf("    -f <email@address.com>            From address\n");
   printf("    -r <email@address.com>            Recipient address\n");
   printf("    -F <domain.com>                   From domain\n");
   printf("    -R <domain.com>                   Recipient domain\n");
   printf("    -s <size>                         Min. size\n");
   printf("    -S <size>                         Max. size\n");
   printf("    -w <where condition>              Where condition to pass to sphinx, eg. \"match('@subject: piler')\"\n");
   printf("    -m <max. matches>                 Max. matches to apply to sphinx query (default: %d)\n", max_matches);
   printf("    -i <index list>                   Sphinx indices to use  (default: %s)\n", index_list);
   printf("    -A                                Reimport all emails from archive\n");
   printf("    -d                                Dry run\n");

   regfree(&regexp);

   exit(0);
}


/* from pilerexport.c
 * no change */
 void p_clean_exit(char *msg, int rc){
   if(msg) printf("error: %s\n", msg);

   if(query) free(query);
   if(logfile) fclose(logfile);

   exit(rc);
}


/* from pilerexport.c
 * no change */
unsigned long convert_time(char *yyyymmdd, int h, int m, int s){
  char *p;
  struct tm tm;

  if(yyyymmdd == NULL) return 0;

  memset((char*)&tm, 0, sizeof(tm));
  tm.tm_isdst = -1;

  tm.tm_hour = h;
  tm.tm_min = m;
  tm.tm_sec = s;

  p = strchr(yyyymmdd, '.'); if(!p) return 0;
  *p = '\0'; tm.tm_year = atoi(yyyymmdd) - 1900; yyyymmdd = p+1;

  p = strchr(yyyymmdd, '.'); if(!p) return 0;
  *p = '\0'; tm.tm_mon = atoi(yyyymmdd) - 1; yyyymmdd = p+1;

  tm.tm_mday = atoi(yyyymmdd);


  tm.tm_isdst = -1;

  return mktime(&tm);
}


/* from pilerexport.c
 * no change */
int append_email_to_buffer(char **buffer, char *email){
  int len, arglen;
  char *s=NULL, emailaddress[SMALLBUFSIZE];

  snprintf(emailaddress, sizeof(emailaddress)-1, "'%s'", email);
  arglen = strlen(emailaddress);

  if(!*buffer){
    *buffer = malloc(arglen+1);
    memset(*buffer, 0, arglen+1);
    memcpy(*buffer, emailaddress, arglen);
  }
  else {
    len = strlen(*buffer);
    s = realloc(*buffer, len + arglen+2);
    if(!s){
      printf("malloc problem!\n");
      return 1;
    }

    *buffer = s;

    memset(*buffer+len, 0, arglen+2);
    strcat(*buffer, ",");
    memcpy(*buffer+len+1, emailaddress, arglen);
  }

  return 0;
}


/* from message.c
 * no changes */
void remove_recipients(struct session_data *sdata, uint64 id){
   char s[SMALLBUFSIZE];

   snprintf(s, sizeof(s)-1, "DELETE FROM " SQL_RECIPIENT_TABLE " WHERE id=%llu", id);

   p_query(sdata, s);
}


/* from pilerexport.c
 * no change */
int append_string_to_buffer(char **buffer, char *str){
  int len, arglen;
  char *s=NULL;

  arglen = strlen(str);

  if(!*buffer){
    *buffer = malloc(arglen+1);
    memset(*buffer, 0, arglen+1);
    memcpy(*buffer, str, arglen);
  }
  else {
    len = strlen(*buffer);
    s = realloc(*buffer, len + arglen+1);
    if(!s) return 1;

    *buffer = s;

    memset(*buffer+len, 0, arglen+1);
    memcpy(*buffer+len, str, arglen);
  }

  return 0;
}


/* from pilerexport.c
 * no change */
uint64 get_total_found(struct session_data *sdata){
  MYSQL_RES *res;
  MYSQL_ROW row;
  uint64 total_found=0;

  if(mysql_real_query(&(sdata->mysql), "SHOW META LIKE 'total_found'", 28) == 0){
    res = mysql_store_result(&(sdata->mysql));
    if(res != NULL){
      while((row = mysql_fetch_row(res))){
        total_found = strtoull(row[1], NULL, 10);
      }
      mysql_free_result(res);
    }
  }

  return total_found;
}

/* from pilerexport.c
 * added attachments column in SQL query
 * changed exportall to reimportall */
int build_query_from_args(char *from, char *to, char *fromdomain, char *todomain, int minsize, int maxsize, unsigned long startdate, unsigned long stopdate){
   int where_condition=1;
   char s[SMALLBUFSIZE];

   if(reimportall == 1){
      rc = append_string_to_buffer(&query, "SELECT `id`, `piler_id`, `digest`, `bodydigest`, `attachments`, `message_id` FROM ");
      rc += append_string_to_buffer(&query, SQL_METADATA_TABLE);
      rc += append_string_to_buffer(&query, " WHERE deleted=0 ");
      return rc;
   }

   snprintf(s, sizeof(s)-1, "SELECT DISTINCT `id`, `piler_id`, `digest`, `bodydigest`, `attachments`, `message_id` FROM %s WHERE deleted=0 ", SQL_MESSAGES_VIEW);

   rc = append_string_to_buffer(&query, s);

   if(from){
      if(where_condition) rc = append_string_to_buffer(&query, " AND ");

      rc += append_string_to_buffer(&query, "`from` IN (");
      rc += append_string_to_buffer(&query, from);
      rc += append_string_to_buffer(&query, ")");

      free(from);

      where_condition++;
   }

   if(to){
      if(where_condition) rc = append_string_to_buffer(&query, " AND ");

      rc += append_string_to_buffer(&query, "`to` IN (");
      rc += append_string_to_buffer(&query, to);
      rc += append_string_to_buffer(&query, ")");

      free(to);

      where_condition++;
   }

   if(fromdomain){
      if(where_condition) rc = append_string_to_buffer(&query, " AND ");

      rc += append_string_to_buffer(&query, "`fromdomain` IN (");
      rc += append_string_to_buffer(&query, fromdomain);
      rc += append_string_to_buffer(&query, ")");

      free(fromdomain);

      where_condition++;
   }


   if(todomain){
      if(where_condition) rc = append_string_to_buffer(&query, " AND ");

      rc += append_string_to_buffer(&query, "`todomain` IN (");
      rc += append_string_to_buffer(&query, todomain);
      rc += append_string_to_buffer(&query, ")");

      free(todomain);

      where_condition++;
   }

   if(minsize > 0){
      if(where_condition) rc = append_string_to_buffer(&query, " AND ");
      snprintf(s, sizeof(s)-1, " `size` >= %d", minsize);
      rc += append_string_to_buffer(&query, s);

      where_condition++;
   }


   if(maxsize > 0){
      if(where_condition) rc = append_string_to_buffer(&query, " AND ");
      snprintf(s, sizeof(s)-1, " `size` <= %d", maxsize);
      rc += append_string_to_buffer(&query, s);

      where_condition++;
   }


   if(startdate > 0){
      if(where_condition) rc = append_string_to_buffer(&query, " AND ");
      snprintf(s, sizeof(s)-1, " `sent` >= %ld", startdate);
      rc += append_string_to_buffer(&query, s);

      where_condition++;
   }


   if(stopdate > 0){
      if(where_condition) rc = append_string_to_buffer(&query, " AND ");
      snprintf(s, sizeof(s)-1, " `sent` <= %ld", stopdate);
      rc += append_string_to_buffer(&query, s);

      where_condition++;
   }


   rc += append_string_to_buffer(&query, " ORDER BY id ASC");

   return rc;
}


/* from pilerexport.c
 * added attachments column in SQL query
 * accept 3 session_data structs (import, export, sphinx)
 * call reimport_emails_matching_to_querry */
uint64 run_query(struct session_data *export_sdata, struct session_data *sphinx_sdata, struct session_data *import_sdata, struct __data *data, char *where_condition, uint64 last_id, int *num, struct __config *cfg){
   MYSQL_RES *res;
   MYSQL_ROW row;
   int rc=0;
   uint64 id=0;
   char s[SMALLBUFSIZE];

   *num = 0;

   if(!where_condition) return id;

   snprintf(s, sizeof(s)-1, "SELECT `id`, `piler_id`, `digest`, `bodydigest`, `attachments`, `message_id` FROM %s WHERE id IN (", SQL_METADATA_TABLE);
   rc += append_string_to_buffer(&query, s);

   snprintf(s, sizeof(s)-1, "SELECT id FROM %s WHERE %s AND id > %llu ORDER BY id ASC LIMIT 0,%d", index_list, where_condition, last_id, max_matches);

   if(mysql_real_query(&(sphinx_sdata->mysql), s, strlen(s)) == 0){
      res = mysql_store_result(&(sphinx_sdata->mysql));
      if(res != NULL){
         while((row = mysql_fetch_row(res))){
            id = strtoull(row[0], NULL, 10);
            (*num)++;
            rc += append_string_to_buffer(&query, row[0]);
            rc += append_string_to_buffer(&query, ",");

         }
         mysql_free_result(res);

         rc += append_string_to_buffer(&query, "-1)");
      }
   }

   if(!rc) reimport_emails_matching_to_query(export_sdata, import_sdata, data, query, cfg);

   free(query);
   query = NULL;


   return id;
}


/* from message.c store_meta_data
 * only have case for old_id > 0
 * do not change retained or folder_id
 * do change message_id
 * return rc of store_recipients
 * */
int update_meta_data(uint64 id, struct session_data *sdata, struct parser_state *state, struct __data *data, struct __config *cfg){
   int rc, ret=ERR, result;
   char *subj, *p, s[MAXBUFSIZE], s2[SMALLBUFSIZE], vcode[2*DIGEST_LENGTH+1], ref[2*DIGEST_LENGTH+1];

   subj = state->b_subject;
   if(*subj == ' ') subj++;

   snprintf(s, sizeof(s)-1, "%llu+%s%s%s%ld%ld%ld%d%d%d%d%s%s%s", id, subj, state->b_from, state->message_id, sdata->now, sdata->sent, sdata->retained, sdata->tot_len, sdata->hdr_len, sdata->direction, state->n_attachments, sdata->ttmpfile, sdata->digest, sdata->bodydigest);

   digest_string(s, &vcode[0]);

   memset(ref, 0, sizeof(ref));
   if(strlen(state->reference) > 10){
      digest_string(state->reference, &ref[0]);
      update_metadata_reference(sdata, state, data, &ref[0], cfg);
   }

   if(prepare_sql_statement(sdata, &(data->stmt_insert_into_meta_table), "UPDATE " SQL_METADATA_TABLE " SET `from`=?,`fromdomain`=?,`subject`=?,`spam`=?,`arrived`=?,`sent`=?,`size`=?,`hlen`=?,`direction`=?,`attachments`=?,`message_id`=?,`reference`=?,`digest`=?,`bodydigest`=?,`vcode`=? WHERE id=?") == ERR) return ERR;

   memset(s2, 0, sizeof(s2));

   p = state->b_from;
   do {
      memset(s2, 0, sizeof(s2));
      p = split(p, ' ', s2, sizeof(s2)-1, &result);

      if(s2[0] == '\0') continue;

      if(does_it_seem_like_an_email_address(s2) == 1){ break; }
   } while(p);


   if(strlen(state->b_to) < 5){
      snprintf(state->b_to, SMALLBUFSIZE-1, "undisclosed-recipients@no.domain");
   }


   p_bind_init(data);

   data->sql[data->pos] = &s2[0]; data->type[data->pos] = TYPE_STRING; data->pos++;
   data->sql[data->pos] = state->b_from_domain; data->type[data->pos] = TYPE_STRING; data->pos++;
   data->sql[data->pos] = subj; data->type[data->pos] = TYPE_STRING; data->pos++;
   data->sql[data->pos] = (char *)&sdata->spam_message; data->type[data->pos] = TYPE_LONG; data->pos++;
   data->sql[data->pos] = (char *)&sdata->now; data->type[data->pos] = TYPE_LONG; data->pos++;
   data->sql[data->pos] = (char *)&sdata->sent; data->type[data->pos] = TYPE_LONG; data->pos++;
   data->sql[data->pos] = (char *)&sdata->tot_len; data->type[data->pos] = TYPE_LONG; data->pos++;
   data->sql[data->pos] = (char *)&sdata->hdr_len; data->type[data->pos] = TYPE_LONG; data->pos++;
   data->sql[data->pos] = (char *)&sdata->direction; data->type[data->pos] = TYPE_LONG; data->pos++;
   data->sql[data->pos] = (char *)&state->n_attachments; data->type[data->pos] = TYPE_LONG; data->pos++;
   data->sql[data->pos] = state->message_id; data->type[data->pos] = TYPE_STRING; data->pos++;
   data->sql[data->pos] = &ref[0]; data->type[data->pos] = TYPE_STRING; data->pos++;
   data->sql[data->pos] = sdata->digest; data->type[data->pos] = TYPE_STRING; data->pos++;
   data->sql[data->pos] = sdata->bodydigest; data->type[data->pos] = TYPE_STRING; data->pos++;
   data->sql[data->pos] = &vcode[0]; data->type[data->pos] = TYPE_STRING; data->pos++;
   data->sql[data->pos] = (char *)&id; data->type[data->pos] = TYPE_LONGLONG; data->pos++;


   if(p_exec_query(sdata, data->stmt_insert_into_meta_table, data) == ERR){
      ret = ERR_EXISTS;
   }
   else {
      remove_recipients(sdata, id);

      rc = store_recipients(sdata, data, state->b_to, id, cfg);
      ret = rc;
      
      if(cfg->verbosity >= _LOG_DEBUG) syslog(LOG_PRIORITY, "%s: stored recipients, rc=%d", sdata->ttmpfile, rc);

      rc = store_index_data(sdata, state, data, id, cfg);

      if(cfg->verbosity >= _LOG_DEBUG) syslog(LOG_PRIORITY, "%s: stored indexdata, rc=%d", sdata->ttmpfile, rc);

   }

   close_prepared_statement(data->stmt_insert_into_meta_table);

   return ret;
}


/* from attachment.c store_attachments
 * check if attachment already stored by piler_id and attachment_id
 * check if already stored attachment has entries pointing to it and different sig/size -> explode
 * if attachment already stored, update it
 * otherwise insert it as in attachment.c
 * */
int store_attachments(struct session_data *sdata, struct parser_state *state, struct __data *data, struct __config *cfg){
   uint64 id=0, old_id=0, old_ptr=0;
   int i, rc=1, found_by_sig, found_by_id, affected_rows, old_size;
   char old_sig[SMALLBUFSIZE];

   if(prepare_sql_statement(sdata, &(data->stmt_get_attachment_id_by_signature), SQL_PREPARED_STMT_GET_ATTACHMENT_ID_BY_SIGNATURE) == ERR) return rc;
   if(prepare_sql_statement(sdata, &(data->stmt_query_attachment), "SELECT `id`,`sig`,`size`,`ptr` FROM `" SQL_ATTACHMENT_TABLE "` WHERE `piler_id`=? AND `attachment_id`=?") == ERR) return rc;

   for(i=1; i<=state->n_attachments; i++){
      found_by_sig = 0;
      found_by_id = 0;
      id = 0;

      if(state->attachments[i].size > 0){

         p_bind_init(data);

         data->sql[data->pos] = sdata->ttmpfile; data->type[data->pos] = TYPE_STRING; data->pos++;
         data->sql[data->pos] = (char *)&i; data->type[data->pos] = TYPE_LONG; data->pos++;

         if(p_exec_query(sdata, data->stmt_query_attachment, data) == OK){

           p_bind_init(data);

           data->sql[data->pos] = (char *)&old_id; data->type[data->pos] = TYPE_LONGLONG; data->len[data->pos] = sizeof(uint64); data->pos++;
           data->sql[data->pos] = old_sig; data->type[data->pos] = TYPE_STRING; data->len[data->pos] = SMALLBUFSIZE; data->pos++;
           data->sql[data->pos] = (char *)&old_size; data->type[data->pos] = TYPE_LONG; data->len[data->pos] = sizeof(uint); data->pos++;
           data->sql[data->pos] = (char *)&old_ptr; data->type[data->pos] = TYPE_LONGLONG; data->len[data->pos] = sizeof(uint64); data->pos++;

           p_store_results(data->stmt_query_attachment, data);
           if(p_fetch_results(data->stmt_query_attachment) == OK) found_by_id = 1;
           p_free_results(data->stmt_query_attachment);
         }

         if(found_by_id){
           // in this case, we have to update, not insert
           if(prepare_sql_statement(sdata, &(data->stmt_insert_into_attachment_table), "UPDATE `" SQL_ATTACHMENT_TABLE "` SET `sig`=?, `name`=?, `type`=?, `size`=?, `ptr`=? WHERE `id`=?") == ERR) return rc;
           if(strncmp(old_sig, state->attachments[i].digest, SMALLBUFSIZE) != 0
              || old_size != state->attachments[i].size){

             // check wether other records point to our attachment
             if(prepare_sql_statement(sdata, &(data->stmt_generic), "SELECT `id` FROM `" SQL_ATTACHMENT_TABLE "` WHERE `ptr`=?") == ERR) return rc;

             p_bind_init(data);

             data->sql[data->pos] = (char *)&old_id, data->type[data->pos] = TYPE_LONGLONG; data->pos++;

             if(p_exec_query(sdata, data->stmt_generic, data) == OK){
                 p_bind_init(data);

                 data->sql[data->pos] = (char *)&id, data->type[data->pos] = TYPE_LONGLONG; data->len[data->pos] = sizeof(uint64); data->pos++;

                 p_store_results(data->stmt_generic, data);
                 if(p_fetch_results(data->stmt_generic) == OK){
                   // There are records pointing to our attachment. Because our sig or size changed, this is bad.
                   // We cannot know if the link is still valid, probably not. So we explode.
                   printf("Attachment id %llu has pointers to it, but sig or size changed. Aborting.\n", old_id);
                   rc = 4;
                   goto CLOSE;
                 }
                 p_free_results(data->stmt_generic); 
                 // We free the statement here, because it was only prepared if the sig or size changed.
                 close_prepared_statement(data->stmt_generic);
               }

           }
         } else {
            // in this case we do the insert as usual
            if(prepare_sql_statement(sdata, &(data->stmt_insert_into_attachment_table), SQL_PREPARED_STMT_INSERT_INTO_ATTACHMENT_TABLE) == ERR) return rc;
         }

         p_bind_init(data);

         data->sql[data->pos] = state->attachments[i].digest; data->type[data->pos] = TYPE_STRING; data->pos++;
         data->sql[data->pos] = (char *)&(state->attachments[i].size); data->type[data->pos] = TYPE_LONG; data->pos++;

         if(p_exec_query(sdata, data->stmt_get_attachment_id_by_signature, data) == OK){

            p_bind_init(data);

            data->sql[data->pos] = (char *)&id; data->type[data->pos] = TYPE_LONGLONG; data->len[data->pos] = sizeof(uint64); data->pos++;

            p_store_results(data->stmt_get_attachment_id_by_signature, data);
            if(p_fetch_results(data->stmt_get_attachment_id_by_signature) == OK) found_by_sig = 1;
            p_free_results(data->stmt_get_attachment_id_by_signature);
         }

         if(found_by_sig && id == old_id){
           found_by_sig = 0;
           id = 0;
         }

         if(!found_by_sig){
           if(store_file(sdata, state->attachments[i].internalname, 0, cfg) == 0){
             syslog(LOG_PRIORITY, "%s: error storing attachment: %s", sdata->ttmpfile, state->attachments[i].internalname);
             goto CLOSE;
           }
         }

         p_bind_init(data);

         if(found_by_id == 0){
           data->sql[data->pos] = sdata->ttmpfile; data->type[data->pos] = TYPE_STRING; data->pos++;
           data->sql[data->pos] = (char *)&i; data->type[data->pos] = TYPE_LONG; data->pos++;
         }
         data->sql[data->pos] = state->attachments[i].digest; data->type[data->pos] = TYPE_STRING; data->pos++;
         data->sql[data->pos] = state->attachments[i].filename; data->type[data->pos] = TYPE_STRING; data->pos++;
         data->sql[data->pos] = state->attachments[i].type; data->type[data->pos] = TYPE_STRING; data->pos++;
         data->sql[data->pos] = (char *)&(state->attachments[i].size); data->type[data->pos] = TYPE_LONG; data->pos++;
         data->sql[data->pos] = (char *)&id; data->type[data->pos] = TYPE_LONGLONG; data->pos++;
         if(found_by_id){
           data->sql[data->pos] = (char *)&old_id; data->type[data->pos] = TYPE_LONGLONG; data->pos++;
         }

         if(p_exec_query(sdata, data->stmt_insert_into_attachment_table, data) == ERR) goto CLOSE;


         affected_rows = p_get_affected_rows(data->stmt_insert_into_attachment_table);
         if(!found_by_id && affected_rows != 1){
            syslog(LOG_PRIORITY, "%s attachment sql error: affected rows: %d", sdata->ttmpfile, affected_rows);
            goto CLOSE;
         }

      }
      else {
         syslog(LOG_PRIORITY, "%s: skipping attachment (serial: %d, size: %d, digest: %s)", sdata->ttmpfile, i, state->attachments[i].size, state->attachments[i].digest);
      }

   }

   rc = 0;

CLOSE:
   close_prepared_statement(data->stmt_insert_into_attachment_table);
   close_prepared_statement(data->stmt_get_attachment_id_by_signature);
   close_prepared_statement(data->stmt_query_attachment);

   return rc;
}

/* from message.c
 * temp rename of message file
 * don't check for duplicate message-id
 * return rc of update_meta_data
 *  */
/* should save message according to id and update metadata table
   also update sphinx index?
*/
int reprocess_message(uint64 id, struct session_data *sdata, struct parser_state *state, struct __data *data, struct __config *cfg){
   int rc, rc2=OK, fd;
   char old_msgfile[SMALLBUFSIZE];

   snprintf(old_msgfile, SMALLBUFSIZE-2, "%s.bak", state->message_id_hash);
   rename(state->message_id_hash, old_msgfile);

   fd = open(state->message_id_hash, O_CREAT|O_EXCL, S_IRUSR|S_IWUSR);
   if(fd == -1){
      remove_stripped_attachments(state);
      if(cfg->verbosity >= _LOG_DEBUG) syslog(LOG_PRIORITY, "%s: touch %s FAILED (%s)", sdata->ttmpfile, state->message_id_hash, state->message_id);
      return ERR_EXISTS;
   }
   close(fd);

   if(cfg->verbosity >= _LOG_DEBUG) syslog(LOG_PRIORITY, "%s: touch %s OK (%s)", sdata->ttmpfile, state->message_id_hash, state->message_id);

   /* store base64 encoded file attachments */

   if(state->n_attachments > 0){
      rc = store_attachments(sdata, state, data, cfg);

      remove_stripped_attachments(state);

      if(rc != 0 && rc != 4) {
        printf("Error storing attachments for %llu.\n", id);
        remove_stored_message_files(sdata, state, cfg);
        rename(old_msgfile, state->message_id_hash);
        return ERR;
      }
      if(rc==4)
        rc2=4;
   }


   rc = store_file(sdata, sdata->tmpframe, 0, cfg);
   if(rc == 0){
      syslog(LOG_PRIORITY, "%s: error storing message: %s", sdata->ttmpfile, sdata->tmpframe);
      remove_stored_message_files(sdata, state, cfg);
      rename(old_msgfile, state->message_id_hash);
      return ERR;
   }


   rc = update_meta_data(id, sdata, state, data, cfg);
   if (rc == 0)
     if(cfg->verbosity >= _LOG_DEBUG) syslog(LOG_PRIORITY, "%s: updated metadata, rc=%d",  sdata->ttmpfile, rc);

   return rc+rc2;
}

/* from import.c import_message
 * file is no stdin
 * set tmpfiles to names derrived from existing piler_id
 * don't check for min_message_size
 * don't check for rules
 * store n_attachments for statistics
 * no duplicate possible (counters code) */
int local_reimport_message(uint64 id, int *n_attachments, char *filename, char *piler_id, struct session_data *sdata, struct __data *data, struct __config *cfg){
   int rc=ERR, fd;
   struct stat st;
   struct parser_state state;
   struct counters counters;


   init_session_data(sdata, cfg);
   snprintf(sdata->ttmpfile, SMALLBUFSIZE-1, "%s", piler_id);
   snprintf(sdata->tmpframe, SMALLBUFSIZE-1, "%s.m", piler_id);

   if(cfg->verbosity > 1) printf("processing: %s\n", filename);

   if(stat(filename, &st) != 0){
     printf("cannot stat() %s\n", filename);
     return rc;
   }

   if(S_ISREG(st.st_mode) == 0){
     printf("%s is not a file\n", filename);
     return rc;
   }

   fd = open(filename, O_RDONLY);
   if(fd == -1){
     printf("cannot open %s\n", filename);
     return rc;
   }
   close(fd);

   snprintf(sdata->filename, SMALLBUFSIZE-1, "%s", filename);

   sdata->tot_len = st.st_size;

   data->import->total_size += sdata->tot_len;

   
   sdata->delivered = 0;

   sdata->import = 1;

   state = parse_message(sdata, 1, data, cfg);
   post_parse(sdata, &state, cfg);

   *n_attachments = state.n_attachments;
   make_digests(sdata, cfg);

   if(sdata->hdr_len < 10){
     printf("%s: invalid message, hdr_len: %d\n", filename, sdata->hdr_len);
     return ERR;
   }

   rc = reprocess_message(id, sdata, &state, data, cfg);

   unlink(state.message_id_hash);
   unlink(sdata->tmpframe);
   unlink(sdata->ttmpfile);

   switch(rc) {
      case OK:
                        if(data->import->reimport == 0){
                           bzero(&counters, sizeof(counters));
                           counters.c_rcvd = 1;
                           counters.c_size += sdata->tot_len;
                           counters.c_stored_size = sdata->stored_len;
                           update_counters(sdata, data, &counters, cfg);
                        }

                        break;


      default:
                        printf("failed to import: %s (id: %s)\n", filename, sdata->ttmpfile);
                        break;
   } 

   return rc;
}

/* from pilerexport.c export_emails_matching_id_list
 * three sdata structs */
void reimport_emails_matching_id_list(struct session_data *export_sdata, struct session_data *sphinx_sdata, struct session_data *import_sdata, struct __data *data, char *where_condition, struct __config *cfg){
   int n;
   uint64 count=0, last_id=0, total_found=0;

   last_id = run_query(export_sdata, sphinx_sdata, import_sdata, data, where_condition, last_id, &n, cfg);
   count += n;

   total_found = get_total_found(sphinx_sdata);

   while(count < total_found){
     last_id = run_query(export_sdata, sphinx_sdata, import_sdata, data, where_condition, last_id, &n, cfg);
      count += n;
   }

}


/* from pilerexport.c export_emails_matching_to_query
 * two sdata structs (import+export) 
 * set filename to tmp name generated from getpid()
 * get old attachment cound from db
 * instead of just exporting:
 *   import file again
 *   delete exported file */
int reimport_emails_matching_to_query(struct session_data *export_sdata, struct session_data *import_sdata, struct __data *data, char *s, struct __config *cfg){
   FILE *f;
   uint64 id, n=0;
   char digest[SMALLBUFSIZE], bodydigest[SMALLBUFSIZE], message_id[SMALLBUFSIZE];
   char filename[SMALLBUFSIZE];
   int rc=0;
   int n_attachments_before, n_attachments_after, export_successfull, import_successfull;
   struct stat st;


   if(prepare_sql_statement(export_sdata, &(data->stmt_generic), s) == ERR) return ERR;


   p_bind_init(data);

   if(p_exec_query(export_sdata, data->stmt_generic, data) == ERR) goto ENDE;

   snprintf(filename, sizeof(&filename)-1, "tmp_%d.eml", getpid());
   if (stat(filename,&st) == 0) {
     unlink(filename);
   }

   p_bind_init(data);

   data->sql[data->pos] = (char *)&id; data->type[data->pos] = TYPE_LONGLONG; data->len[data->pos] = sizeof(uint64); data->pos++;
   data->sql[data->pos] = export_sdata->ttmpfile; data->type[data->pos] = TYPE_STRING; data->len[data->pos] = RND_STR_LEN; data->pos++;
   data->sql[data->pos] = &digest[0]; data->type[data->pos] = TYPE_STRING; data->len[data->pos] = sizeof(digest)-2; data->pos++;
   data->sql[data->pos] = &bodydigest[0]; data->type[data->pos] = TYPE_STRING; data->len[data->pos] = sizeof(bodydigest)-2; data->pos++;
   data->sql[data->pos] = (char *)&n_attachments_before; data->type[data->pos] = TYPE_LONG; data->pos++;
   data->sql[data->pos] = &message_id[0]; data->type[data->pos] = TYPE_STRING; data->len[data->pos] = sizeof(message_id)-2; data->pos++;

   p_store_results(data->stmt_generic, data);

   while(p_fetch_results(data->stmt_generic) == OK){

      if(id > 0){

         if(dryrun == 0){

            f = fopen(filename, "w");
            if(f){
               export_successfull = 0;
               import_successfull = 0;

               rc = retrieve_email_from_archive(export_sdata, data, f, cfg);
               fclose(f);

               if (rc == 0) {

                 snprintf(export_sdata->filename, SMALLBUFSIZE-1, "%s", filename);

                 make_digests(export_sdata, cfg);


                 if(strcmp(digest, export_sdata->digest) == 0 && strcmp(bodydigest, export_sdata->bodydigest) == 0){
                   if (cfg->verbosity >= _LOG_DEBUG) syslog(LOG_PRIORITY, "Digests match after export.");
                   export_successfull = 1;

                   rc = local_reimport_message(id, &n_attachments_after, filename, export_sdata->ttmpfile, import_sdata, data, cfg);
                   import_successfull = rc == 0;

                   if ((n_attachments_before == 0) && (n_attachments_after == 0)) {
                     if (cfg->verbosity >= _LOG_DEBUG) syslog(LOG_PRIORITY, "Still zero attachments: %llu.", id);
                   } else if (n_attachments_before == n_attachments_after) {
                     if (cfg->verbosity >= _LOG_DEBUG) syslog(LOG_PRIORITY, "Still some attachments: %llu.", id);
                   } else if (n_attachments_before < n_attachments_after) {
                     syslog(LOG_PRIORITY, "Attachments increased for: %llu.", id);
                   } else {
                     syslog(LOG_PRIORITY, "Attachments decreased for: %llu.", id);
                   }

                   if(import_successfull){
                     n++;
                     printf("reimported: %10llu\r", n); fflush(stdout);
                   }
                 }
                 else
                   printf("verification FAILED. %s\n", export_sdata->ttmpfile);

               }
               else
                 printf("cannot retrieve message id %llu\n", id);

               fprintf(logfile, "%llu,%s,%s,%s,%d,%s,%s,%d,%d,%d\n", id, message_id, digest, bodydigest, n_attachments_before, export_sdata->digest, export_sdata->bodydigest, n_attachments_after, export_successfull, import_successfull);

            }
            else printf("cannot open: %s\n", filename);

            unlink(filename);

            if(!import_successfull)
              break;
         }
         else {
            printf("id:%llu\n", id);
         }

      }

   }

   p_free_results(data->stmt_generic);

ENDE:
   close_prepared_statement(data->stmt_generic);


   printf("\n");

   return rc;
}


/* mostly from pilerexprt.c
 * naming of sdata structs
 * logfile
 * */
int main(int argc, char **argv){
   int c, minsize=0, maxsize=0;
   size_t nmatch=0;
   unsigned long startdate=0, stopdate=0;
   char *configfile=CONFIG_FILE;
   char *to=NULL, *from=NULL, *todomain=NULL, *fromdomain=NULL, *where_condition=NULL;
   struct session_data export_sdata, import_sdata, sphinx_sdata;
   struct __data data;
   struct __config cfg;
   struct import import;


   if(regcomp(&regexp, "^([\\+a-z0-9_\\.@\\-]+)$", REG_ICASE | REG_EXTENDED)){
      p_clean_exit("cannot compile rule!", 1);
   }


   while(1){

#ifdef _GNU_SOURCE
      static struct option long_options[] =
         {
            {"config",       required_argument,  0,  'c' },
            {"minsize",      required_argument,  0,  's' },
            {"maxsize",      required_argument,  0,  'S' },
            {"all",          no_argument,        0,  'A' },
            {"dry-run",      no_argument,        0,  'd' },
            {"dryrun",       no_argument,        0,  'd' },
            {"help",         no_argument,        0,  'h' },
            {"version",      no_argument,        0,  'v' },
            {"from",         required_argument,  0,  'f' },
            {"to",           required_argument,  0,  'r' },
            {"from-domain",  required_argument,  0,  'F' },
            {"to-domain",    required_argument,  0,  'R' },
            {"start-date",   required_argument,  0,  'a' },
            {"stop-date",    required_argument,  0,  'b' },
            {"where-condition", required_argument,  0,  'w' },
            {"max-matches",  required_argument,  0,  'm' },
            {"index-list",   required_argument,  0,  'i' },
            {0,0,0,0}
         };

      int option_index = 0;

      c = getopt_long(argc, argv, "c:s:S:f:r:F:R:a:b:w:m:i:Adhv?", long_options, &option_index);
#else
      c = getopt(argc, argv, "c:s:S:f:r:F:R:a:b:w:m:i:Adhv?");
#endif

      if(c == -1) break;

      switch(c){

         case 'c' :
                    configfile = optarg;
                    break;

         case 's' :
                    minsize = atoi(optarg);
                    break;

         case 'S' :
                    maxsize = atoi(optarg);
                    break;


         case 'A' :
                    reimportall = 1;
                    break;

         case 'f' :

                    if(regexec(&regexp, optarg, nmatch, NULL, 0)){
                       printf("%s is not a valid email address\n", optarg);
                       break;
                    }

                    rc = append_email_to_buffer(&from, optarg);

                    break;

         case 'r' :

                    if(regexec(&regexp, optarg, nmatch, NULL, 0)){
                       printf("%s is not a valid email address\n", optarg);
                       break;
                    }

                    rc = append_email_to_buffer(&to, optarg);

                    break;

         case 'F' :

                    if(regexec(&regexp, optarg, nmatch, NULL, 0)){
                       printf("%s is not a valid domain name\n", optarg);
                       break;
                    }

                    rc = append_email_to_buffer(&fromdomain, optarg);

                    break;           

         case 'R' :

                    if(regexec(&regexp, optarg, nmatch, NULL, 0)){
                       printf("%s is not a valid domain name\n", optarg);
                       break;
                    }

                    rc = append_email_to_buffer(&todomain, optarg);

                    break;                     
                    
         case 'a' :
                    startdate = convert_time(optarg, 0, 0, 0);
                    break;


         case 'b' :
                    stopdate = convert_time(optarg, 23, 59, 59);
                    break;

         case 'w' :
                    where_condition = optarg;
                    break;

         case 'm' :
                    max_matches = atoi(optarg);
                    break;

         case 'i' :
                    index_list = optarg;
                    break;

         case 'd' :
                    dryrun = 1;
                    break;

         default  :
                    usage();
                    break;
      }

   }


   if(from == NULL && to == NULL && fromdomain == NULL && todomain == NULL && where_condition == NULL && startdate == 0 && stopdate == 0 && reimportall == 0) usage();


   regfree(&regexp);

   if(!can_i_write_current_directory()) __fatal("cannot write current directory!");

   (void) openlog("pilerreimport", LOG_PID, LOG_MAIL);
   logfile = fopen("reimport.log", "a");

   /* stuff from pilerimport */
   srand(getpid());

   data.folder = 0;
   data.recursive_folder_names = 0;
   data.quiet = 0;


   import.import_job_id = import.total_messages = import.total_size = import.processed_messages = import.batch_processing_limit = 0;
   import.started = import.updated = import.finished = import.remove_after_import = import.reimport = 0;
   import.extra_recipient = import.move_folder = NULL;
   import.start_position = 1;
   import.download_only = 0;
   import.timeout = 30;

   cfg = read_config(configfile);
   /* make sure we don't discard messages without a valid Message-Id when importing manually */
   cfg.archive_emails_not_having_message_id = 1;

   data.import = &import;

   if(open_database(&import_sdata, &cfg) == ERR){
     p_clean_exit("cannot connect to mysql server", 1);
   }

   inithash(data.mydomains);
   load_mydomains(&import_sdata, &data, &cfg);

   if(read_key(&cfg)) p_clean_exit(ERR_READING_KEY, 1);

   setlocale(LC_CTYPE, cfg.locale);
   /* end pilerimport */


   init_session_data(&export_sdata, &cfg);


   if(open_database(&export_sdata, &cfg) == ERR){
      p_clean_exit("cannot connect to mysql server", 1);
   }


   if(where_condition){

      init_session_data(&sphinx_sdata, &cfg);

      strcpy(cfg.mysqlhost, "127.0.0.1");
      cfg.mysqlport = 9306;
      cfg.mysqlsocket[0] = '\0';

      if(open_database(&sphinx_sdata, &cfg) == ERR){
         p_clean_exit("cannot connect to 127.0.0.1:9306", 1);
      }

      reimport_emails_matching_id_list(&export_sdata, &sphinx_sdata, &import_sdata, &data, where_condition, &cfg);

      close_database(&sphinx_sdata);
   }
   else {
      if(build_query_from_args(from, to, fromdomain, todomain, minsize, maxsize, startdate, stopdate) > 0) p_clean_exit("malloc problem building query", 1);
      reimport_emails_matching_to_query(&export_sdata, &import_sdata, &data, query, &cfg);
      free(query);
   }

   close_database(&export_sdata);
   close_database(&import_sdata);
   clearhash(data.mydomains);

   return rc;
}
