<?php


class ControllerSearchMoveFolder extends Controller {
   private $error = array();

   public function index(){

      $this->id = "content";
      $this->template = "search/tag.tpl";
      $this->layout = "common/layout-empty";

      $session = Registry::get('session');
      $request = Registry::get('request');
      $db = Registry::get('db');

      $this->load->model('search/search');
      $this->load->model('folder/folder');

      if(isset($this->request->post['folder_id']) && isset($this->request->post['idlist'])) {
         $idlist = explode(",", $this->request->post['idlist']);

         if(count($idlist) > 0) {

            // TODO: This doesn't work, why?
            //$ids = $this->model_search_search->check_your_permission_by_id_list($idlist);

            $folder_id = $this->request->post['folder_id'];

            foreach ($idlist as $id) {
               $this->model_folder_folder->update_message_folder($id, $folder_id);
	    }
         }
      }
   }

}

?>
