<?php


class ControllerMessageSpam extends Controller {
   private $error = array();

   public function index(){

      $this->id = "content";
      $this->template = "search/tag.tpl";
      $this->layout = "common/layout-empty";

      $session = Registry::get('session');
      $request = Registry::get('request');
      $db = Registry::get('db');

      $this->load->model('search/search');
      $this->load->model('folder/folder');
      $this->load->model('search/search');
      $this->load->model('search/message');
      $this->load->model('audit/audit');
      $this->load->model('user/user');
      $this->load->model('mail/mail');

      if(isset($this->request->post['is_spam']) && isset($this->request->post['idlist'])) {
         $idlist = explode(",", $this->request->post['idlist']);
         $is_spam = $this->request->post['is_spam'];

         if(count($idlist) > 0) {
            
            if(in_array($is_spam, ["true", "false"])) {

               $is_spam = in_array($is_spam, ["true"]);

               if($is_spam) {
                  if(defined('REPORT_SPAM_ADDRESS')) {
                     $mailto = [ REPORT_SPAM_ADDRESS ];
                  }
                  $folder_id = FOLDER_ID_SPAM;
               } else {
                  if(defined('REPORT_HAM_ADDRESS')) {
                     $mailto = [ REPORT_HAM_ADDRESS ];
                  }
                  $folder_id = FOLDER_ID_HAM;
               }

               foreach ($idlist as $id) {
                  AUDIT(ACTION_REPORT_SPAM, '', '', $id, '');

                  if(!$this->model_search_search->check_your_permission_by_id($id)) {
                     AUDIT(ACTION_UNAUTHORIZED_VIEW_MESSAGE, '', '', $id, '');
                     die("no permission for " . $id);
                  }

                  if(!verify_piler_id($id)) {
                     AUDIT(ACTION_UNKNOWN, '', '', $id, 'unknown piler id: ' . $id);
                     die("invalid id: " . $id);
                  }

                  $this->model_folder_folder->update_message_folder($id, $folder_id);

                  if(isset($mailto)){
                     $this->forward_mail($id, $mailto);
                  }
               }

            }
         }
      }
   }

   private function forward_mail($id, $rcpt) {

      $this->document->title = $this->data['text_message'];

      $piler_id = $this->model_search_message->get_piler_id_by_id($id);

      $this->model_search_message->connect_to_pilergetd();
      $msg = $this->model_search_message->get_raw_message($piler_id);
      $this->model_search_message->disconnect_from_pilergetd();

      $this->model_search_message->remove_journal($msg);

      $x = $this->model_mail_mail->send_smtp_email(SMARTHOST, SMARTHOST_PORT, SMTP_DOMAIN, SMTP_FROMADDR, $rcpt, $msg );

      return $x;

   }

}

?>
